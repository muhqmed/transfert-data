// Connect to pg database : 
const { Pool } = require('pg')
const mongoose = require('mongoose');
const Formation = require('./models/Formation.js')
const Professions = require('./models/Professions');
const ActivitySectors = require('./models/ActivitySectors.js')
const Skills = require('./models/Skills.js')
const Structures = require('./models/Structures.js');

// pools will use environment variables
// for connection information
const pool = new Pool({
    user: 'ukvca1em36ugnh7btsip',
    host: 'b3uu7ujdva1gsexr34bn-postgresql.services.clever-cloud.com',
    database: 'b3uu7ujdva1gsexr34bn',
    password: 'p0feIiDvWoh8mjl67TV0',
    port: 5432,
})
async function main() {
    //Remove Localhost when running script -- 
    const url = 'mongodb://localhost:27017/base-platform-dev'
    console.log("Connecting to MongoDB...");
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then(result => {
        console.log("Connetion established sucessfully.");
    })
    GetFormation();
}
async function GetFormation() {
    // SQL Request to get all formation
    // Remove LIMIT when execute the script in the production
    let queryFormation = 'SELECT * FROM formations LIMIT 20';
    // let queryFormation = process.env.isProduction ? 'SELECT * FROM formations' : 'SELECT * FROM formations LIMIT 2' ;

    // SQL to JOIN Each formation with his structures
    let queryStructure = 'SELECT label from formations JOIN formation_structures ON formations.id = formation_structures.formation_id\
  JOIN structures ON structures.id = formation_structures.structure_id WHERE formation_id = ';

    // SQL to JOIN Each formation with his professions
    let queryProfession = 'SELECT libelle from formations JOIN formation_professions ON formations.id = formation_professions.formation_id\
  JOIN professions ON professions.id = formation_professions.profession_id WHERE formation_id = ';

    // SQL to JOIN Each formation with his Activity Sectors
    let queryActivitySector = 'SELECT label from formations JOIN activity_sectors_formations ON formations.id = activity_sectors_formations.formation_id\
  JOIN activity_sectors ON activity_sectors.id = activity_sectors_formations.activity_sector_id WHERE formation_id = ';

  // SQL to JOIN Each formation with his Skills - PROBLEM IS :
  /* 
    in Ruby formation SQL Schemas whe have SKills by savoit Type : 
    in mongoose schema we have skills by savoirType and 

    Skills by Specs - Exemple : in each skills we specify if it Base Skills or Specific Skills or Etre Skills
 
  */
    let querySkills = 'SELECT libelle FROM formations\
  JOIN formation_skills ON formations.id = formation_skills.formation_id\
  JOIN skills ON skills.id = formation_skills.skill_id WHERE formation_id = '

  console.log("Getting Formations from PostgreSQL");
    var formations = await pool.query(queryFormation);
   
    const keys = Object.values(formations.rows);
    console.log(`${keys.length} formations Getted Sucessfully !`);
    let counter = 0;
    for (let i = 0; i < keys.length; i++) {
        let f = keys[i];
        //Profession Data
        let professionLibelle = await pool.query(queryProfession + f.id);
        let formationText = []
        let professionId = []
        for (let i = 0; i < professionLibelle.rows.length; i++) {
            const element = professionLibelle.rows[i];
            formationText.push(element.libelle)
        }
        let profession = await Professions.find({
            libelle: formationText
        });
        profession.forEach(profession => {
            professionId.push(profession._id)
        })
        // Stucure data
        let structureLibelle = await pool.query(queryStructure + f.id);
        let structureText = [];
        let structureId = []

        for (let i = 0; i < structureLibelle.rows.length; i++) {
            const element = structureLibelle.rows[i];
            structureText.push(element.label)
        }
        let structure = await Structures.find({
            label: structureText
        });
        structure.forEach(structure => {
            structureId.push(structure._id)
        })

        // Activity Sector Data
        let SectorLibelle = await pool.query(queryActivitySector + f.id);
        let sectorText = [];
        let sectorId = []

        for (let i = 0; i < SectorLibelle.rows.length; i++) {
            const element = SectorLibelle.rows[i];
            sectorText.push(element.label)
        }
        let activitySector = await ActivitySectors.find({
            label: sectorText
        });
        activitySector.forEach(activitySector => {
            sectorId.push(activitySector._id)
        })

        // Skills Data
        let skillsLibelle = await pool.query(querySkills + f.id)
        let skillsText = []
        let skillsId = []

        for (let i = 0; i < skillsLibelle.rows.length; i++) {
            const element = skillsLibelle.rows[i];
            skillsText.push(element.libelle);
        }
        let skills = await Skills.find({
            libelle : skillsText
        })
        // get Skills type : 
        let skillsIdSavoir = []
        let skillsIdSavoirFaire = []
        let skillsIdSavoirEtre = []

        skills.forEach(skill => {
            if (skill.savoirType == 'Savoir') {
                skillsIdSavoir.push(skill._id)
            } else if (skill.savoirType == 'SavoirFaire') {
                skillsIdSavoirFaire.push(skill._id)
            } else if (skill.savoirType == 'Savoir Etre') {
                skillsIdSavoirEtre.push(skill._id)
            }
        })
        skills.forEach(skill => {
            skillsId.push(skill._id)
        })
        // Why not use ENUM ?
        var rncpText = '';
            if (f.level == 3) {
                rncpText = 'CAP, BEP';
            } else if (f.level == 4) {
                 rncpText = 'Baccalauréat';
            } else if (f.level == 5) {
                 rncpText = 'BTS, DEUG, DUT, DEUST';
            } else if (f.level == 6) {
                 rncpText = 'Liscence, Liscence LMD';
            } else if (f.level == 7) {
                 rncpText = 'Master';
            } else {
                 rncpText = 'Pas definie';
            }
        let formation = new Formation({
            name: f.name,
            description: f.activities,
            level: f.level,
            active: f.active,
            rncpCode: rncpText,
            activitySectorsId: sectorId,
            professionsId: professionId,
            skillsId: {
                base: {
                    savoir: skillsIdSavoir,
                    savoirFaire: skillsIdSavoirFaire
                },
                specific: {
                    savoir: skillsIdSavoir,
                    savoirFaire: skillsIdSavoirFaire
                },
                etre: skillsIdSavoirEtre
            },
            structuresId: structureId,
            formationInitiale: f.formation_initiale,
            apprentissage: f.apprentissage,
            formationContinue: f.formation_continue,
            contratProfessionnalisation: f.contrat_professionnalisation,
            certificatorName: f.certificator_name,
            certificatorWebsite: f.certificator_website,
            formatorMail: f.formatorMail,
            treated: f.treated,
            localisation: f.localisation,
            alternance: f.alternance,
            stage: f.stage,
            stageDateBeginning: f.stage_date_beginning,
            diplomType: f.diplom_type,
            stageDateEnding: f.stage_date_ending,
            languages: f.languages,
            formationDistance: f.formation_distance,
            formationPresentiel: f.formation_presentiel,
            formationHybride: f.formation_hybride
        });
        // console.log("this is formation : ", formation);
        await formation.save();
        counter ++;
        console.log(`You Have Added ${counter} Formation Successfully ! `);
    }
    console.log("->>> Formation Found : ", keys.length);
    console.log("->>> Formation Added : ", counter);
}

main();
