// import mongoose from 'mongoose';
const mongoose = require('mongoose')

const Professions = new mongoose.Schema(
  {
    libelle: {
      type: String,
      text: true
    }
    // romeId: {
    //   type: mongoose.Types.ObjectId
    // }
  },
  { timestamps: true }
);

module.exports =  mongoose.model('Professions', Professions);
