const mongoose = require('mongoose')

const ActivitySectors = new mongoose.Schema(
  {
    label: {
      type: String,
      required: true,
      text: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('ActivitySectors', ActivitySectors);
