const mongoose = require('mongoose')

const Skills = new mongoose.Schema(
  {
    libelle: {
      type: String,
      text: true // recuperer de l'api de pole emplois
    },
    ogrCode: {
      type: Number
    },
    savoirType: {
      type: String // Savoir  ou  savoir etre ou savoir etre
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Skills', Skills);
