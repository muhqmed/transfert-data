const mongoose = require('mongoose')

const Structures = new mongoose.Schema(
  {
    label: {
      type: String,
      text: true
    }
  },
  { timestamps: true }
);

module.exports =  mongoose.model('Structures', Structures);
