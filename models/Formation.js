// import mongoose from 'mongoose';
const mongoose = require('mongoose')


const FormationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      text: true
    },
    description: {
      type: String
    },
    level: {
      type: Number
    },
    active: {
      type: Boolean
    },
    rncpCode: {
      // repertoire national de la certification professionnel
      type: String
    },
    activitySectorsId: [
      {
        type: mongoose.Types.ObjectId, // imprimerie de test
        ref: 'ActivitySectors'
      }
    ],
    professionsId: [
      {
        // get id of professions schema
        type: mongoose.Types.ObjectId,
        ref: 'Professions'
      }
    ],
    skillsId: {
      base: {
        savoir: [
          {
            type: mongoose.Types.ObjectId,
            ref: 'Skills'
          }
        ],
        savoirFaire: [
          {
            type: mongoose.Types.ObjectId,
            ref: 'Skills'
          }
        ]
      },
      specific: {
        savoir: [
          {
            type: mongoose.Types.ObjectId,
            ref: 'Skills'
          }
        ],
        savoirFaire: [
          {
            type: mongoose.Types.ObjectId,
            ref: 'Skills'
          }
        ]
      },
      etre: [
        {
          type: mongoose.Types.ObjectId,
          ref: 'Skills'
        }
      ]
    },
    structuresId: [
      {
        type: mongoose.Types.ObjectId,
        ref: 'Structures'
      }
    ],
    formationInitiale: {
      type: Boolean
    },
    apprentissage: {
      type: Boolean
    },
    formationContinue: {
      type: Boolean
    },
    contratProfessionnalisation: {
      type: Boolean
    },
    certificatorName: [
      {
        type: String
      }
    ],
    certificatorWebsite: {
      type: String
    },
    formatorMail: {
      type: String
    },
    treated: {
      type: Boolean
    },
    localisation: {
      type: String
    },
    alternance: {
      type: Boolean
    },
    stage: {
      type: Boolean
    },
    stageDateBeginning: {
      type: String
    },
    diplomType: {
      type: String
    },
    stageDateEnding: {
      type: String
    },
    languages: {
      type: String
    },
    formationDistance: {
      type: Boolean
    },
    formationPresentiel: {
      type: Boolean
    },
    formationHybride: {
      type: Boolean
    }
  },
  { timestamps: true }
);
module.exports = mongoose.model('Formations', FormationSchema);
