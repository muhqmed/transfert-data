create_table "activity_sectors", force: :cascade do |t|
  t.string "label" 
  #t.datetime "created_at", null: false
  #t.datetime "updated_at", null: false
  t.string "normalized_label"
end

create_table "activity_sectors_formations", id: false, force: :cascade do |t|
  t.bigint "activity_sector_id", null: false
  t.bigint "formation_id", null: false
end

create_table "activity_types", force: :cascade do |t|
  t.string "label"
  #t.datetime "created_at", null: false
  #t.datetime "updated_at", null: false
end

create_table "formation_professions", force: :cascade do |t|
  t.bigint "formation_id"
  t.bigint "profession_id"
  t.integer "priority"
  #t.datetime "created_at", null: false 
  #t.datetime "updated_at", null: false
  t.index ["formation_id"], name: "index_formation_professions_on_formation_id"
  t.index ["profession_id"], name: "index_formation_professions_on_profession_id"
end

create_table "formation_skills", force: :cascade do |t|
  t.bigint "formation_id"
  t.bigint "skill_id"
  t.integer "priority"
  #t.datetime "created_at", null: false
  #t.datetime "updated_at", null: false
  t.bigint "activity_type_id"
  t.index ["activity_type_id"], name: "index_formation_skills_on_activity_type_id"
  t.index ["formation_id"], name: "index_formation_skills_on_formation_id"
  t.index ["skill_id"], name: "index_formation_skills_on_skill_id"
end

create_table "formation_structures", force: :cascade do |t|
  t.bigint "formation_id"
  t.bigint "structure_id"
  #t.datetime "created_at", null: false
  #t.datetime "updated_at", null: false
  t.integer "priority"
  t.index ["formation_id"], name: "index_formation_structures_on_formation_id"
  t.index ["structure_id"], name: "index_formation_structures_on_structure_id"
end

create_table "formations", force: :cascade do |t|
  t.string "name"
  t.text "description"
  #t.datetime "created_at", null: false
  #t.datetime "updated_at", null: false
  t.integer "level"
  t.boolean "active"
  t.string "rncp_code"
  t.text "activity_sector"
  t.text "jobs"
  t.text "activities"
  t.text "skills_text"
  t.boolean "formation_initiale"
  t.boolean "apprentissage"
  t.boolean "formation_continue"
  t.boolean "contrat_professionnalisation"
  t.string "certificator_name"
  t.string "certificator_website"
  t.string "formator_mail"
  t.boolean "treated"
  t.string "localisation"
  t.string "normalized_name"
  t.string "normalized_certificator_name"
  t.string "normalized_localisation"
  t.boolean "alternance"
  t.boolean "stage"
  t.string "stage_date_beginning"
  t.string "diplom_type"
  t.string "normalized_diplom_type"
  t.string "stage_date_ending"
  t.string "languages"
  t.boolean "formation_distance"
  t.boolean "formation_presentiel"
  t.boolean "formation_hybride"
end

create_table "formations_rome_codes", id: false, force: :cascade do |t|
  t.bigint "formation_id", null: false
  t.bigint "rome_code_id", null: false
end

create_table "formations_romes", id: false, force: :cascade do |t|
  t.bigint "rome_id", null: false
  t.bigint "formation_id", null: false
end

create_table "formations_teaching_languages", id: false, force: :cascade do |t|
  t.bigint "formation_id", null: false
  t.bigint "teaching_language_id", null: false
end

create_table "formations_users", id: false, force: :cascade do |t|
  t.bigint "formation_id", null: false
  t.bigint "user_id", null: false
end

create_table "identified_candidates", force: :cascade do |t|
  t.bigint "student_id"
  t.bigint "recrutement_id"
  t.boolean "recruited"
  t.boolean "contacted"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.index ["recrutement_id"], name: "index_identified_candidates_on_recrutement_id"
  t.index ["student_id"], name: "index_identified_candidates_on_student_id"
end

create_table "identified_formations", force: :cascade do |t|
  t.bigint "formation_id"
  t.bigint "recrutement_id"
  t.boolean "evaluated"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.index ["formation_id"], name: "index_identified_formations_on_formation_id"
  t.index ["recrutement_id"], name: "index_identified_formations_on_recrutement_id"
end

create_table "pending_rates", force: :cascade do |t|
  t.bigint "skill_id"
  t.bigint "formation_id"
  t.integer "rate"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.bigint "identified_candidate_id"
  t.string "user_company_size"
  t.index ["formation_id"], name: "index_pending_rates_on_formation_id"
  t.index ["identified_candidate_id"], name: "index_pending_rates_on_identified_candidate_id"
  t.index ["skill_id"], name: "index_pending_rates_on_skill_id"
end

create_table "professions", force: :cascade do |t|
  t.string "libelle"
  t.bigint "rome_id"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.string "normalized_libelle"
  t.index ["rome_id"], name: "index_professions_on_rome_id"
end

create_table "ratings", force: :cascade do |t|
  t.bigint "identified_candidate_id"
  t.bigint "formation_skill_id"
  t.integer "rate"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.string "user_company_size"
  t.index ["formation_skill_id"], name: "index_ratings_on_formation_skill_id"
  t.index ["identified_candidate_id"], name: "index_ratings_on_identified_candidate_id"
end

create_table "recrutement_basket_items", force: :cascade do |t|
  t.bigint "recrutement_id"
  t.bigint "skill_id"
  t.bigint "profession_id"
  t.string "organisme"
  t.string "localisation"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.boolean "important_skill"
  t.index ["profession_id"], name: "index_recrutement_basket_items_on_profession_id"
  t.index ["recrutement_id"], name: "index_recrutement_basket_items_on_recrutement_id"
  t.index ["skill_id"], name: "index_recrutement_basket_items_on_skill_id"
end

create_table "recrutements", force: :cascade do |t|
  t.string "status"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.string "title"
  t.bigint "user_id"
  t.boolean "evaluated"
  t.date "eval_counter_end_date"
  t.index ["user_id"], name: "index_recrutements_on_user_id"
end

create_table "reviews", force: :cascade do |t|
  t.text "description"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.bigint "formation_id"
  t.index ["formation_id"], name: "index_reviews_on_formation_id"
end

create_table "rome_codes", force: :cascade do |t|
  t.string "code"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
end

create_table "romes", force: :cascade do |t|
  t.string "code"
  t.text "description"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
end

create_table "romes_skills", id: false, force: :cascade do |t|
  t.bigint "rome_id", null: false
  t.bigint "skill_id", null: false
end

create_table "savoir_types", force: :cascade do |t|
  t.string "label"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
end

create_table "skills", force: :cascade do |t|
  t.string "libelle"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.integer "ogr_code"
  t.string "normalized_libelle"
  t.string "savoir_type"
end

create_table "structures", force: :cascade do |t|
  t.string "label"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
end

create_table "students", force: :cascade do |t|
  t.string "name"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.bigint "formation_id"
  t.string "email"
  t.bigint "user_id"
  t.index ["formation_id"], name: "index_students_on_formation_id"
  t.index ["user_id"], name: "index_students_on_user_id"
end

create_table "teaching_languages", force: :cascade do |t|
  t.string "label"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
end

create_table "users", force: :cascade do |t|
  t.string "email", default: "", null: false
  t.string "encrypted_password", default: "", null: false
  t.string "reset_password_token"
  t.datetime "reset_password_sent_at"
  t.datetime "remember_created_at"
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  t.string "company"
  t.bigint "recrutement_id"
  t.string "first_name"
  t.string "last_name"
  t.string "account_type"
  t.string "organisme"
  t.string "company_size"
  t.string "confirmation_token"
  t.datetime "confirmed_at"
  t.datetime "confirmation_sent_at"
  t.string "unconfirmed_email"
  t.boolean "search_stage"
  t.boolean "search_contrat_professionnalisation"
  t.boolean "search_job"
  t.boolean "search_apprentissage"
  t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  t.index ["email"], name: "index_users_on_email", unique: true
  t.index ["recrutement_id"], name: "index_users_on_recrutement_id"
  t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
end

add_foreign_key "formation_professions", "formations"
add_foreign_key "formation_professions", "professions"
add_foreign_key "formation_skills", "activity_types"
add_foreign_key "formation_skills", "formations"
add_foreign_key "formation_skills", "skills"
add_foreign_key "formation_structures", "formations"
add_foreign_key "formation_structures", "structures"
add_foreign_key "identified_candidates", "recrutements"
add_foreign_key "identified_candidates", "students"
add_foreign_key "identified_formations", "formations"
add_foreign_key "identified_formations", "recrutements"
add_foreign_key "pending_rates", "formations"
add_foreign_key "pending_rates", "identified_candidates"
add_foreign_key "pending_rates", "skills"
add_foreign_key "professions", "romes"
add_foreign_key "ratings", "formation_skills"
add_foreign_key "ratings", "identified_candidates"
add_foreign_key "recrutement_basket_items", "professions"
add_foreign_key "recrutement_basket_items", "recrutements"
add_foreign_key "recrutement_basket_items", "skills"
add_foreign_key "recrutements", "users"
add_foreign_key "reviews", "formations"
add_foreign_key "students", "formations"
add_foreign_key "students", "users"
add_foreign_key "users", "recrutements"

